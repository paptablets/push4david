//version 1.0.1
const domain = self.location.hostname;
//console.log(domain);
async function swActivate() {
    await self.clients.claim();
}
async function swPush(e) {

    const t = JSON.parse(e.data.text());

    let options = {
        body: t.body,
        icon: t.icon,
        data: t.data,
        tag: t.tag,

    };
    // options.data.clickurl = getClickUrl(t);
    e.waitUntil(self.registration.showNotification(t.title, options));
}

async function swNotificationClick(e) {
    try {
        const t = e.notification;
        if ((t.close(), e.action)) {
            const a = t.actions.find(t => t.action === e.action);
            await handleAction(a);
        } else {

            const c = navigateTo(e);
            await Promise.all([c]);
        }
    } catch (err) {
        console.log(err);
    }
}
async function navigateTo(e) {
    return self.clients.openWindow(e.notification.data.clickurl);
}

self.addEventListener("activate", e => e.waitUntil(swActivate()));
self.addEventListener("push", e => e.waitUntil(swPush(e)));
self.addEventListener("notificationclick", e =>
    e.waitUntil(swNotificationClick(e))
);

self.addEventListener("pushsubscriptionchange", function (
    registration,
    newSubscription,
    oldSubscription
) {
    savePushSubscription(newSubscription);
});
